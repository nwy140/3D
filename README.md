# 3D

3D Models 

#Blender WorkFlow
Import reference image
Create a low poly model after the image, decide how your topology will be like, use mirror modifier to reduce workload

Mark edges that are sharp and edges as seam for uv unwrapping. Use TextureAtlas addon for UVUnwrapping

Create a high poly model after the image, use subdivision surface for high poly and use edge loops to alter the topology by placing them near the edges

Then, create CageModel low poly model and apply displace modifier to it, make sure its surrounds the high poly model

Select all faces that has more than 4 vertices and reduce them using tris to quad (Alt + J)

Then, export low poly, high poly and caged model one by one.

Open up XNormal, select high poly mesh, low poly mesh and caged mesh.
Setup Normal settings, then export normal.

3D Coat
Import the normal exported from XNormal and low poly mesh. Then, Texture Paint over the mesh you imported. 
Then export it using ue4 preset. Import to ue4 , and in material set texture samples to exported images textures. Everything works!!!
